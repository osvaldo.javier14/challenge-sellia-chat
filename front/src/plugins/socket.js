import { io } from 'socket.io-client';

let socket;

const createSocket = (username) => {
  if (socket) {
    socket.disconnect();
  }

  socket = io('ws://localhost:3005', {
    auth: {
      username,
    },
  });

  return socket;
};

const getSocket = () => socket;

export { createSocket, getSocket };
