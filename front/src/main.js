import { createApp } from 'vue';
import App from './App.vue';
import { createSocket, getSocket } from './plugins/socket';

const app = createApp(App);

app.config.globalProperties.$createSocket = createSocket;
app.config.globalProperties.$getSocket = getSocket;

app.mount('#app');
