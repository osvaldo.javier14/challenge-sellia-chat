export class SearchContentUseCase {
  constructor(adapter) {
    this.adapter = adapter;
  }

  async execute(content, startPk) {
    return await this.adapter.searchContent(content, startPk);
  }
}
