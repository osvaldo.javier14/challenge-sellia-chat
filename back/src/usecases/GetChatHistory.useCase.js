export class GetChatHistoryUseCase {
  constructor(adapter) {
    this.adapter = adapter;
  }

  async execute(startPk, endPk, limit, modeReverse) {
    return await this.adapter.getChatHistoryBetweenPks(
      startPk,
      endPk,
      limit,
      modeReverse
    );
  }
}
