export class SendMessageUseCase {
  constructor(adapter) {
    this.adapter = adapter;
  }

  async execute(msg, username, clientOffset) {
    return await this.adapter.insertChatMessage(msg, username, clientOffset);
  }
}
