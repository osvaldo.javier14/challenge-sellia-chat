import express from 'express';
import cors from 'cors';
import { createServer } from 'node:http';
import { fileURLToPath } from 'node:url';
import { dirname, join } from 'node:path';
import { availableParallelism } from 'node:os';
import cluster from 'node:cluster';
import { setupPrimary } from '@socket.io/cluster-adapter';

import { setupControllers } from './controllers/index.js';

if (cluster.isPrimary) {
  const numCPUs = availableParallelism();
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork({
      PORT: 3000 + i,
    });
  }

  setupPrimary();
} else {
  const port = process.env.PORT;
  const app = express();
  const server = createServer(app);

  const chatController = setupControllers(server);

  const __dirname = dirname(fileURLToPath(import.meta.url));

  app.use((req, res, next) => {
    req.io = chatController.socketAdapter.io;
    next();
  });

  app.get('/chat', (req, res) => {
    res.sendFile(join(__dirname, 'index.html'));
  });

  // app.get('/history', (req, res) => chatController.handleGetHistory(req, res));
  // app.get('/search', (req, res) => chatController.handleGetHistory(req, res));

  server.listen(port, () => {
    console.log(`server running at http://localhost:${port}`);
  });
}
