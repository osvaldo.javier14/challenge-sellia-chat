import { mongoAdapter } from '../adapters/database/index.js';
import { SocketAdapter } from '../adapters/socket/Socket.adapter.js';
import { ChatController } from './Chat.controller.js';

export const setupControllers = (server) => {
  const socketAdapter = new SocketAdapter(server);
  const chatController = new ChatController(mongoAdapter, socketAdapter);

  socketAdapter.onConnection((socket) =>
    chatController.handleConnection(socket)
  );

  return chatController;
};
