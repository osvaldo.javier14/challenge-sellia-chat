import { GetChatHistoryUseCase } from '../usecases/GetChatHistory.useCase.js';
import { SendMessageUseCase } from '../usecases/SendMessage.useCase.js';
import { SearchContentUseCase } from '../usecases/SearchContent.useCase.js';
import { validateText } from '../utils/Strings.util.js';

export class ChatController {
  constructor(adapter, socketAdapter) {
    this.getChatHistoryUseCase = new GetChatHistoryUseCase(adapter);
    this.sendMessageUseCase = new SendMessageUseCase(adapter);
    this.searchContentUseCase = new SearchContentUseCase(adapter);
    this.socketAdapter = socketAdapter;
    this.userSockets = new Map();
    this.CHAT_HISTORY_LIMIT = 4;
  }

  async handleSendMessage(socket, msg, clientOffset, callback) {
    const username = socket.handshake.auth.username;

    if (!this.validateUsername(socket, username, callback)) return;
    if (!this.validateMessage(socket, msg, callback)) return;

    try {
      const row = await this.sendMessageUseCase.execute(
        msg,
        username,
        clientOffset
      );
      this.socketAdapter.io.emit('chat message', row);
      callback();
    } catch (e) {
      this.handleSendMessageError(e, callback);
    }
  }

  validateUsername(socket, username, callback) {
    if (!username || username === 'undefined' || username === 'null') {
      socket.emit('chat message error', {
        content: 'Necesitas un nombre de usuario para enviar mensajes.',
        atTheStart: false,
      });
      callback();
      return false;
    }
    return true;
  }

  validateMessage(socket, msg, callback) {
    if (!validateText(msg)) {
      socket.emit('chat message error', {
        content: 'El mensaje debe contener solo letras y espacios.',
        atTheStart: false,
      });
      callback();
      return false;
    }
    return true;
  }

  handleSendMessageError(e, callback) {
    if (e.errno === 19) {
      // SQLITE_CONSTRAINT
      callback();
    } else {
      console.error(e);
    }
  }

  async handleLoadHistory(socket, startPk) {
    const username = socket.handshake.auth.username;
    const socketId = this.userSockets.get(username);

    let startOffset = Math.max(startPk - this.CHAT_HISTORY_LIMIT, 0);

    if (startOffset <= 0) {
      this.socketAdapter.emitToUser(socketId, 'chat message error', {
        content: 'No hay más historial.',
        atTheStart: true,
      });
      return;
    }

    await this.sendChatHistory(socketId, startOffset, startPk);
  }

  async sendChatHistory(socketId, startOffset, startPk) {
    let chats = await this.getChatHistoryUseCase.execute(startOffset, startPk);
    chats.reverse().forEach((row) => {
      this.socketAdapter.emitToUser(socketId, 'chat message history', row);
    });
  }

  async handleSearch(socket, content, startPk, callback) {
    const username = socket.handshake.auth.username;
    const socketId = this.userSockets.get(username);

    const chats = await this.searchContentUseCase.execute(content, startPk);

    if (chats.length === 0) {
      this.socketAdapter.emitToUser(socketId, 'chat message error', {
        content: 'No se encontraron mensajes.',
        atTheStart: false,
      });
      callback();
      return;
    }

    const pk = chats[0]?.pk || 0;
    await this.sendChatHistory(socketId, pk, startPk);
    callback();
  }

  async handleConnection(socket) {
    const username = socket.handshake.auth.username;

    this.initializeUser(socket, username);
    console.log(`👉 User connected: ${username}`);

    if (!socket.recovered) {
      await this.loadInitialHistory(socket, socket.handshake.auth.serverOffset);
    }

    socket.on('chat message', (msg, clientOffset, callback) => {
      this.handleSendMessage(socket, msg, clientOffset, callback);
    });

    socket.on('chat message history', (startPk) => {
      this.handleLoadHistory(socket, startPk);
    });

    socket.on('chat message search', (content, startPk, callback) => {
      this.handleSearch(socket, content, startPk, callback);
    });

    socket.on('disconnect', () => {
      console.log(`👉 User disconnected ${username}`);
      this.userSockets.delete(username);
    });
  }

  initializeUser(socket, username) {
    this.userSockets.set(username, socket.id);
  }

  async loadInitialHistory(socket, startPk) {
    try {
      startPk = Number(startPk) || 0;
      const endPk = -1;
      const modeReverse = true;
      let chatHistory = await this.getChatHistoryUseCase.execute(
        startPk,
        endPk,
        this.CHAT_HISTORY_LIMIT,
        modeReverse
      );

      chatHistory.reverse().forEach((row) => {
        socket.emit('chat message', row);
      });
    } catch (error) {
      console.error(error);
    }
  }
}
