import boom from '@hapi/boom';

export const formatCodeError = (error, code, message = '') => {
  // convertir el error a tipo boom si no lo es
  if (!error.isBoom) {
    error = boom.boomify(error);
  }

  error.output.payload.codeValue = code.toUpperCase();
  if (message !== '') {
    error.output.payload.message = message;
  }

  return error;
};

export const validateText = (input) => {
  const regex = /^[a-zA-Z\s]+$/; // minus, mayus, blank space
  return regex.test(input);
};
