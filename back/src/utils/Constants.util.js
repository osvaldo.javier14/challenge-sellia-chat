export const constants = {
  MONGO: {
    MODELS: {
      CHAT_HISTORY: 'archi-chat-history',
      AUTO_INCREMENT: 'archi-auto-increment',
    },
  },
};
