import { Server } from 'socket.io';
import { createAdapter } from '@socket.io/cluster-adapter';

export class SocketAdapter {
  constructor(server) {
    this.io = new Server(server, {
      connectionStateRecovery: {},
      adapter: createAdapter(),
      cors: {
        origin: 'http://localhost:5173',
      },
    });
  }

  emitToUser(socketId, event, msg, newStartPk) {
    this.io.to(socketId).emit(event, msg, newStartPk);
  }

  onConnection(callback) {
    this.io.on('connection', callback);
  }
}
