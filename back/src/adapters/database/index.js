import { mongoAdapter } from './Mongo.adapter.js';

(async () => {
  await mongoAdapter.connect();
})();

export { mongoAdapter };
