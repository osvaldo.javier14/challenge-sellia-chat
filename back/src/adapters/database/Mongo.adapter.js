import mongoose from 'mongoose';

import config from '../../config/config.js';
import { constants } from '../../utils/Constants.util.js';

const NAME_MODEL_CHAT_HISTORY = constants.MONGO.MODELS.CHAT_HISTORY;
const NAME_MODEL_AUTO_INCREMENT = constants.MONGO.MODELS.AUTO_INCREMENT;

const schemaOptionsChatHistory = {
  collection: NAME_MODEL_CHAT_HISTORY,
  versionKey: 'version',
  timestamps: true,
};

const schemaOptionsAutoIncrement = {
  collection: NAME_MODEL_AUTO_INCREMENT,
  versionKey: 'version',
  timestamps: true,
};

const chatMessageSchema = new mongoose.Schema(
  {
    pk: Number, // autoincrement
    date: {
      type: Date,
      default: Date.now,
    },
    content: String,
    username: String,
    clientOffset: String,
  },
  schemaOptionsChatHistory
);

const autoIncrementSchema = new mongoose.Schema(
  {
    seq: Number,
    field: String,
  },
  schemaOptionsAutoIncrement
);

chatMessageSchema.pre('save', async function preSaveHook(next) {
  const chat = this;
  if (this.isNew) {
    let autoincrement = await AutoIncrementModel.findOneAndUpdate(
      { field: 'pk' },
      { $inc: { seq: 1 } },
      { new: true }
    )
      .lean()
      .exec();

    if (!autoincrement) {
      autoincrement = await AutoIncrementModel.create({
        field: 'pk',
        seq: 1,
      });
    }

    // set the autoincrement id
    chat.pk = autoincrement.seq;
  }

  next();
});

const ChatMessageModel = mongoose.model(
  NAME_MODEL_CHAT_HISTORY,
  chatMessageSchema
);
const AutoIncrementModel = mongoose.model(
  NAME_MODEL_AUTO_INCREMENT,
  autoIncrementSchema
);

class MongoAdapter {
  constructor() {
    this.url = config.dbSelliaURI;
  }

  async connect() {
    await mongoose.connect(this.url);
  }

  async disconnect() {
    await mongoose.disconnect();
  }

  async getChatHistoryBetweenPks(startPk, endPk, limit, modeReverse) {
    console.log(
      '🚀 ~ MongoAdapter ~ getChatHistoryBetweenPks ~ startPk, endPk, limit, modeReverse:',
      startPk,
      endPk,
      limit,
      modeReverse
    );
    let chatMessages = ChatMessageModel.find({
      pk: { $gte: startPk },
    });

    if (endPk > 0) {
      chatMessages = chatMessages.find({
        pk: { $lt: endPk },
      });
    }

    if (limit) {
      chatMessages.limit(limit);
    }

    if (modeReverse) {
      chatMessages.sort({ pk: -1 });
    }

    chatMessages = await chatMessages.exec();
    return chatMessages;
  }

  async insertChatMessage(msg, username, clientOffset) {
    const chatMessage = new ChatMessageModel({
      username,
      content: msg,
      clientOffset,
      date: new Date(),
    });

    await chatMessage.save();
    return chatMessage;
  }

  async searchContent(content, startPk) {
    let chatMessages = ChatMessageModel.find({
      content: new RegExp(content, 'i'),
      pk: { $lt: startPk },
    });

    // obtener solo el primer mensaje
    chatMessages = chatMessages.limit(1);

    return await chatMessages.exec();
  }
}

export const mongoAdapter = new MongoAdapter();
