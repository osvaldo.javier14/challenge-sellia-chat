/* eslint-disable no-undef */

const DEV = 'dev';
const env = process.env.NODE_ENV || DEV;

const config = {
  env,
  isDev: env === DEV,
  apiVersionV1: '1.0.1',
  dbSelliaURI: process.env.DB_SELLIA_URI || 'mongodb://localhost:27017/sellia',
  jwtSecret: process.env.JWT_SECRET || 'secret_fake_jwt',
  port: process.env.PORT || 3005,
};

export default config;
