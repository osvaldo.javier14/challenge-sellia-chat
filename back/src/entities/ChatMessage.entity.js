export class ChatMessage {
  constructor(pk, username, content, date) {
    this.pk = pk;
    this.username = username;
    this.content = content;
    this.date = date;
  }
}
