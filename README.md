# Hola ✨
Este proyecto Full Stack implementa conceptos de Clean Architecture, WebSockets, persistencia en bases de datos, y contenerización. El frontend está diseñado para consumir eventos generados por el backend de manera eficiente. Se ha escrito código escalable y mantenible, asegurando una rigurosa separación de responsabilidades tanto en el frontend como en el backend.

Se recomienda instalar y usar Docker para evitar una instalación manual de Mongo.

# Un poco más...

- Para aplicar todos los conceptos, se limitó la carga inicial del chat a los últimos cuatro mensajes enviados por todos los usuarios. A medida que se envían nuevos mensajes, estos se agregan al contenedor, el cual tiene una altura máxima para permitir el uso del scroll.

- El botón "Cargar historial" se activa cuando hay más de cuatro mensajes en el chat. Al actualizar la página y presionar este botón, se cargan los mensajes anteriores, también con un límite de cuatro mensajes a la vez.

- El botón "Buscar" permite realizar búsquedas de fragmentos de palabras en toda la base de datos. Devuelve los mensajes donde ocurre la primera aparición del término buscado y los carga los mensajes en el contenedor del chat.

## Screenshots
![App Screenshot](https://gitlab.com/osvaldo.javier14/challenge-sellia-chat/-/raw/main/public/chat.png)


## Prerrequisitos
- Docker Compose (Backend)
- Node v20.13.1 (Frontend)

## Environment Variables

Para ejecutar este proyecto, deberá crear un archivo en `back/env/dev/.env` y añadir las siguientes variables de entorno a su archivo .env

```bash
  NODE_ENV='dev'
  DB_SELLIA_URI='mongodb://sellia_mongo_container_dev/sellia'
```


## Run Locally

### Backend (Nodejs + SocketIo + Mongodb)
1) Clonar el projecto.
2) Entrar a la carpeta `back`
3) Crear el archivo .env si no lo has hecho ya
4) Construir la imagen con docker compose
5) Levantar la imagen con docker compose

```bash
  git clone https://gitlab.com/osvaldo.javier14/challenge-sellia-chat.git
  cd challenge-sellia-chat/back
  docker compose build
  docker compose up
```

### Frontend (Vuejs)
1) Clonar el projecto.
2) Entrar a la carpeta `front`
3) Instalar las dependencias
4) Iniciar el servidor

```bash
  git clone https://gitlab.com/osvaldo.javier14/challenge-sellia-chat.git
  cd challenge-sellia-chat/front
  npm i
  npm run dev
```

# Gracias
Totales